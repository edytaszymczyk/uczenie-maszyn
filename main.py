from utilitis.iohandler import read_csv, write_to_csv
from utilitis.tools import split_data, do_cross_validation, split_into_x_and_y, transpose_array
from algorithms.algorithm import cluster_centroids, random, edited_nn, tomek_links, classifier_knn
from scipy import stats
import numpy as np


def generate_files_with_metrics(file):
    file_data = read_csv(file + ".csv")
    X, y = split_data(file_data)
    f1_metric = []
    precission_metric = []
    recall_metric = []
    train_array, test_array = do_cross_validation(5, X, y)
    for train_instance, test_instance in zip(train_array, test_array):
        X, y = split_into_x_and_y(train_instance)
        X_test, y_test = split_into_x_and_y(test_instance)
        Xc, yc, _ = cluster_centroids(X, y)
        # Xr, yr, _ = random(X, y)
        # Xe, ye, _ = edited_nn(X, y)
        # Xt, yt, _ = tomek_links(X, y)

        f1, recall, precision = classifier_knn(Xc, yc, X_test, y_test)
        f1_metric.append(f1)
        recall_metric.append(recall)
        precission_metric.append(precision)

    file_data_to_csv = []
    file_data_to_csv.append(f1_metric)
    file_data_to_csv.append(recall_metric)
    file_data_to_csv.append(precission_metric)
    header = ["f1", "recall", "precision"]
    transpose_file = transpose_array(file_data_to_csv)
    write_to_csv(file + "_metrics_cc.csv", transpose_file, header)


def generate_files_by_metrics(file):
    suffixes = ["_all", "_cc", "_rand", "_enn", "_tl"]
    f1_list = []
    recall_list = []
    precision_list = []
    for suffix in suffixes:
        file_with_metrics = read_csv(file + "_metrics" + suffix + ".csv")
        data_as_array = file_with_metrics.values
        f1 = data_as_array[:, 0]
        recall = data_as_array[:, 1]
        precision = data_as_array[:, 3]
        f1_list.append(f1)
        recall_list.append(recall)
        precision_list.append(precision)
    f1_nparray = np.asarray(f1_list, dtype=np.float32)
    recall_nparray = np.asarray(recall_list, dtype=np.float32)
    precision_nparray = np.asarray(precision_list, dtype=np.float32)
    header = ["1", "2", "3", "4", "5"]
    write_to_csv(file + "_f1" +".csv", f1_nparray, header)
    write_to_csv(file + "_recall" +".csv", recall_nparray, header)
    write_to_csv(file + "_precision" +".csv", precision_nparray, header)

# array_by_metric, np. precision_nparray from generate_files_by_metrics
def generate_file_with_test(file, array_by_metric):
    wilcoxon_matrix = []
    wilcoxon_row = []
    for i in range(0, 5):
        for j in range(0, 5):
            first = array_by_metric[i]
            second = array_by_metric[j]
            if i == j or (first == second).all():
                wilcoxon_row.append(1)
            else:
                stat, p = stats.wilcoxon(first, second)
            wilcoxon_row.append(p)
        wilcoxon_matrix.append(wilcoxon_row)
        wilcoxon_row = []
    header = ["cały zbiór", "CC", "RUS", "ENN", "TL"]
    write_to_csv(file + "_test.csv", np.asarray(wilcoxon_matrix, dtype=np.float32), header)


def main():
    files = ["abalone19", "page-blocks0", "shuttle-2_vs_5", "segment0", "occupancy"]
    for file in files:
        generate_files_with_metrics(file)


if __name__ == "__main__":
    main()
