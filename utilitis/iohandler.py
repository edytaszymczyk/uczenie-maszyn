# package installation: pip install --user pandas / pip install pandas in administrator mode
import pandas as pd

data_directory = '../data/'


def read_csv(filename):
    path = data_directory + filename
    data = pd.read_csv(path)
    return data


def write_to_csv(filename, data, header):
    path = data_directory + filename
    df = pd.DataFrame(data=data)
    df.to_csv(path, encoding='utf-8', index=False, sep=',', header=header)
    return 0
