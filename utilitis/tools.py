from sklearn import model_selection
import numpy as np


def split_data(data):
    data_set = data.values
    X = data_set[:, :-1]
    y = data_set[:, -1]
    return X, y


def split_into_x_and_y(data_set):
    X = data_set[:, :-1]
    y = data_set[:, -1]
    return X, y


def transpose_array(array):
    array_reshaped = np.reshape(array, (len(array), -1))
    return array_reshaped.T


def merge_lists(X, y):
    y_reshaped = np.reshape(y, (-1, len(y)))
    merged_data = np.concatenate((X, y_reshaped.T), axis=1)
    return merged_data


def do_cross_validation(k, X, y):
    cv = model_selection.StratifiedKFold(n_splits=k, random_state=None)
    train_array = []
    test_array = []
    for train, test in cv.split(X, y):
        X_train, y_train = X[train], y[train]
        X_test, y_test = X[test], y[test]

        train_instance = merge_lists(X_train, y_train)
        test_instance = merge_lists(X_test, y_test)

        train_array.append(train_instance)
        test_array.append(test_instance)
    return train_array, test_array


def calculate_reduction_measure(base_X, X_after_reduction):
    base_number_of_objects = base_X.shape[0]
    number_of_objects_after_reduction = X_after_reduction.shape[0]
    return ((base_number_of_objects - number_of_objects_after_reduction) / base_number_of_objects) * 100
