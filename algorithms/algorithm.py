from imblearn.under_sampling import ClusterCentroids, EditedNearestNeighbours, TomekLinks, RandomUnderSampler
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from utilitis.tools import merge_lists, split_data


def cluster_centroids(X, y):
    cc = ClusterCentroids(random_state=0)
    X_resampled, y_resampled = cc.fit_resample(X, y)
    merged_data = merge_lists(X_resampled, y_resampled)
    return X_resampled, y_resampled, merged_data


def random(X, y):
    rus = RandomUnderSampler(random_state=0)
    X_resampled, y_resampled = rus.fit_resample(X, y)
    merged_data = merge_lists(X_resampled, y_resampled)
    return X_resampled, y_resampled, merged_data


def tomek_links(X, y):
    tl = TomekLinks()
    X_resampled, y_resampled = tl.fit_resample(X, y)
    merged_data = merge_lists(X_resampled, y_resampled)
    return X_resampled, y_resampled, merged_data


def edited_nn(X, y):
    enn = EditedNearestNeighbours()
    X_resampled, y_resampled = enn.fit_resample(X, y)
    merged_data = merge_lists(X_resampled, y_resampled)
    return X_resampled, y_resampled, merged_data


def classifier_knn(X_train, y_train, X_test, y_test):
    classifier = KNeighborsClassifier(n_neighbors=1)
    classifier.fit(X_train, y_train)
    y_predict = classifier.predict(X_test)
    f1 = metrics.f1_score(y_test, y_predict, pos_label="positive", average="binary")
    recall = metrics.recall_score(y_test, y_predict, pos_label="positive", average="binary")
    precision = metrics.precision_score(y_test, y_predict, pos_label="positive", average="binary")
    return f1, recall, precision
